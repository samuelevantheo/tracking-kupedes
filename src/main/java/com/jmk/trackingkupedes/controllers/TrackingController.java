package com.jmk.trackingkupedes.controllers;

import com.jmk.trackingkupedes.services.TrackingServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/tracking-kupedes")
public class TrackingController {
    private final TrackingServices trackingServices;

    @Autowired
    public TrackingController(TrackingServices trackingServices){
        this.trackingServices = trackingServices;
    }

    @PostMapping("/track")
    public ResponseEntity<Map<String, Object>> getTrackingData (@RequestParam("noRekening") String noRekening, @RequestParam("tipe") String tipe){
        Map<String, Object> responseHandling = new HashMap<>();
        String parameter;
        switch (tipe){
            case "noPeserta":
                parameter = "nomor peserta";
                break;
            case "noRekening":
                parameter = "nomor rekening";
                break;
            default:
                parameter = "tidak dikenal";
                break;
        }
        try{
            Map<String, Object> dataNasabah = trackingServices.getDataNasabah(noRekening, tipe);
            Map<String, Object> dataPenjaminan = trackingServices.getDataPenjaminan(noRekening, tipe);
            Map<String, Object> dataKlaim = trackingServices.getDataKlaim(noRekening, tipe);

            if (dataNasabah != null){
                responseHandling.put("responseCode", "00");
                responseHandling.put("responseDescription", "Success");
                responseHandling.put("responseException", "");
                responseHandling.put("dataNasabah", dataNasabah);
                responseHandling.put("dataPenjaminan", dataPenjaminan);
                responseHandling.put("dataKlaim", dataKlaim);

                return ResponseEntity.ok(responseHandling);
            }else{
                switch (tipe){
                    case "noRekening" :
                        responseHandling.put("responseCode", "01");
                        responseHandling.put("responseDescription", "Failed");
                        responseHandling.put("responseException", "Tidak ada data atas " + parameter + " : " + noRekening);
                        break;
                    case "noPeserta":
                        responseHandling.put("responseCode", "01");
                        responseHandling.put("responseDescription", "Failed");
                        responseHandling.put("responseException", "Tidak ada data atas " + parameter + " : " + noRekening);
                        break;
                    default:
                        responseHandling.put("responseCode", "01");
                        responseHandling.put("responseDescription", "Failed");
                        responseHandling.put("responseException", "Parameter tipe " + tipe + " tidak dikenali. ");
                }

                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseHandling);
            }
        } catch (Exception e){
            responseHandling.put("responseCode", "01");
            responseHandling.put("responseDescription", "Failed");
            responseHandling.put("responseException", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseHandling);
        }
    }
}
