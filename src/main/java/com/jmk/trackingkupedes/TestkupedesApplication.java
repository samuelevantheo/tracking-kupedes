package com.jmk.trackingkupedes;

import  org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestkupedesApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestkupedesApplication.class, args);
    }

}
