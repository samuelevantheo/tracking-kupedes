package com.jmk.trackingkupedes.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TrackingServices {
    private final JdbcTemplate jdbcTemplate;
    @Autowired
    public TrackingServices(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public Map<String, Object> getDataNasabah(String noRekening, String tipe){
        String sql;

        switch(tipe){
            case "noRekening" :
                sql = "SELECT\n" +
                        "a.id_opmt_pengajuan_tamp,\n" +
                        "c.nama,\n" +
                        "a.kode_uker,\n" +
                        "e.nama_bank_cabang,\n" +
                        "g.ket_wil_kerja,\n" +
                        "a.no_ktp,\n" +
                        "a.npwp\n" +
                        "FROM opmt_pengajuan_tamp a \n" +
                        "INNER JOIN opmt_transaksi_penjaminan b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "INNER JOIN opmt_nasabah c ON c.id_opmt_nasabah = b.id_opmt_nasabah\n" +
                        "INNER JOIN opmt_permohonan d ON d.id_opmt_permohonan = b.id_opmt_permohonan\n" +
                        "INNER JOIN dd_bank_cabang e ON d.id_dd_bank_cabang = e.id_dd_bank_cabang\n" +
                        "INNER JOIN dd_bank f ON f.id_dd_bank = e.id_dd_bank\n" +
                        "INNER JOIN dc_wilayah_kerja g WITH (nolock) on g.id_dc_wilayah_kerja = d.id_dc_wilayah_kerja\n" +
                        "WHERE 1=1\n" +
                        "AND f.id_dd_bank = 15\n" +
                        "AND a.no_rekening = ? \n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC;";
                break;
            case "noPeserta" :
                 sql = "SELECT\n" +
                        "a.id_opmt_pengajuan_tamp,\n" +
                        "c.nama,\n" +
                        "a.kode_uker,\n" +
                        "e.nama_bank_cabang,\n" +
                        "g.ket_wil_kerja,\n" +
                        "a.no_ktp,\n" +
                        "a.npwp\n" +
                        "FROM opmt_pengajuan_tamp a \n" +
                        "INNER JOIN opmt_transaksi_penjaminan b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "INNER JOIN opmt_nasabah c ON c.id_opmt_nasabah = b.id_opmt_nasabah\n" +
                        "INNER JOIN opmt_permohonan d ON d.id_opmt_permohonan = b.id_opmt_permohonan\n" +
                        "INNER JOIN dd_bank_cabang e ON d.id_dd_bank_cabang = e.id_dd_bank_cabang\n" +
                        "INNER JOIN dd_bank f ON f.id_dd_bank = e.id_dd_bank\n" +
                        "INNER JOIN dc_wilayah_kerja g WITH (nolock) on g.id_dc_wilayah_kerja = d.id_dc_wilayah_kerja\n" +
                        "WHERE 1=1\n" +
                        "AND f.id_dd_bank = 15\n" +
                        "AND a.nomor_peserta = ? \n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC;";
                 break;
            default:
                 sql = "Tidak ada data";
                 break;
        }

        List<Map<String, Object>> result;

        if(!sql.equals("Tidak ada data")) {
            result = jdbcTemplate.queryForList(sql, noRekening);
        }else{
            result = new ArrayList<>();
        }

        if (!result.isEmpty()){
            return result.get(0);
        }else{
            return null;
        }
    }
    public Map<String, Object> getDataPenjaminan(String noRekening, String tipe){
        String sql;
        switch(tipe) {
            case"noRekening":
                sql = "SELECT\n" +
                        "g.no_rekening as nomorRekeningPinjaman,\n" +
                        "a.nomor_peserta as nomorPeserta,\n" +
                        "b.id_opmt_transaksi_penjaminan,\n" +
                        "'-' AS omsetUsaha,\n" +
                        "a.created_date as periodePenerimaan,\n" +
                        "h.jenis_kur as jenisKur,\n" +
                        "a.peruntukan_kredit as peruntukanKredit,\n" +
                        "a.nilai_ijp,\n" +
                        "a.nomor_sk,\n" +
                        "a.tgl_sk,\n" +
                        "a.tgl_realisasi,\n" +
                        "a.tgl_awal as tanggalMulai,\n" +
                        "a.tgl_jatuh_tempo,\n" +
                        "a.jangka_waktu,\n" +
                        "CASE WHEN a.flag_satuan_jangka_waktu = 1 THEN 'Minggu' WHEN a.flag_satuan_jangka_waktu = 3 THEN 'TAHUN' ELSE 'Bulan' END as satuanWaktu,\n" +
                        "a.pokok_pembiayaan,\n" +
                        "CASE\n" +
                        "\tWHEN LEN(a.FIDProgram) > 2 THEN 'BRISURF'\n" +
                        "\tELSE 'NON-BRISURF'\n" +
                        "END AS covering_source,\n" +
                        "a.FIDProgram,\n" +
                        "a.NamaProgram\n" +
                        "FROM opmt_pengajuan_tamp a\n" +
                        "INNER JOIN opmt_transaksi_penjaminan b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "INNER JOIN opmt_nasabah c ON c.id_opmt_nasabah = b.id_opmt_nasabah\n" +
                        "INNER JOIN opmt_permohonan d ON d.id_opmt_permohonan = b.id_opmt_permohonan\n" +
                        "INNER JOIN dd_bank_cabang e ON d.id_dd_bank_cabang = e.id_dd_bank_cabang\n" +
                        "INNER JOIN dd_bank f ON f.id_dd_bank = e.id_dd_bank\n" +
                        "LEFT JOIN opmt_rekening_pinjaman g ON g.id_opmt_transaksi_penjaminan = b.id_opmt_transaksi_penjaminan\n" +
                        "INNER JOIN dd_jenis_kur h ON h.id_dd_jenis_kur = a.id_dd_jenis_kur\n" +
                        "WHERE 1=1\n" +
                        "AND f.id_dd_bank = 15\n" +
                        "AND a.no_rekening = ?\n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC";
                break;
            case "noPeserta" :
                sql = "SELECT\n" +
                        "g.no_rekening as nomorRekeningPinjaman,\n" +
                        "a.nomor_peserta as nomorPeserta,\n" +
                        "b.id_opmt_transaksi_penjaminan,\n" +
                        "'-' AS omsetUsaha,\n" +
                        "a.created_date as periodePenerimaan,\n" +
                        "h.jenis_kur as jenisKur,\n" +
                        "a.peruntukan_kredit as peruntukanKredit,\n" +
                        "a.nilai_ijp,\n" +
                        "a.nomor_sk,\n" +
                        "a.tgl_sk,\n" +
                        "a.tgl_realisasi,\n" +
                        "a.tgl_awal as tanggalMulai,\n" +
                        "a.tgl_jatuh_tempo,\n" +
                        "a.jangka_waktu,\n" +
                        "CASE WHEN a.flag_satuan_jangka_waktu = 1 THEN 'Minggu' WHEN a.flag_satuan_jangka_waktu = 3 THEN 'TAHUN' ELSE 'Bulan' END as satuanWaktu,\n" +
                        "a.pokok_pembiayaan,\n" +
                        "CASE\n" +
                        "\tWHEN LEN(a.FIDProgram) > 2 THEN 'BRISURF'\n" +
                        "\tELSE 'NON-BRISURF'\n" +
                        "END AS covering_source,\n" +
                        "a.FIDProgram,\n" +
                        "a.NamaProgram\n" +
                        "FROM opmt_pengajuan_tamp a\n" +
                        "INNER JOIN opmt_transaksi_penjaminan b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "INNER JOIN opmt_nasabah c ON c.id_opmt_nasabah = b.id_opmt_nasabah\n" +
                        "INNER JOIN opmt_permohonan d ON d.id_opmt_permohonan = b.id_opmt_permohonan\n" +
                        "INNER JOIN dd_bank_cabang e ON d.id_dd_bank_cabang = e.id_dd_bank_cabang\n" +
                        "INNER JOIN dd_bank f ON f.id_dd_bank = e.id_dd_bank\n" +
                        "LEFT JOIN opmt_rekening_pinjaman g ON g.id_opmt_transaksi_penjaminan = b.id_opmt_transaksi_penjaminan\n" +
                        "INNER JOIN dd_jenis_kur h ON h.id_dd_jenis_kur = a.id_dd_jenis_kur\n" +
                        "WHERE 1=1\n" +
                        "AND f.id_dd_bank = 15\n" +
                        "AND a.nomor_peserta = ?\n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC";
                break;
            default:
                sql = "Tidak ada data";
                break;
        }

        List<Map<String, Object>> result;

        if(!sql.equals("Tidak ada data")) {
            result = jdbcTemplate.queryForList(sql, noRekening);
        }else{
            result = new ArrayList<>();
        }

        if (!result.isEmpty()){
            return result.get(0);
        }else{
            return null;
        }
    }

    public Map<String, Object> getDataKlaim(String noRekening, String tipe){
        String sql;
        switch(tipe) {
            case"noRekening":
                sql = "SELECT\n" +
                        "a.nomor_peserta,\n" +
                        "b.date_created AS periodeKlaimDiterima,\n" +
                        "b.nilai_tuntutan_klaim,\n" +
                        "b.tunggakan_pokok,\n" +
                        "b.tunggakan_bunga,\n" +
                        "b.tunggakan_denda,\n" +
                        "b.nilai_payoff_total,\n" +
                        "CASE WHEN b.persetujuan_klaim = 2 THEN 'SETUJU' ELSE 'TOLAK' END AS keputusanKlaim,\n" +
                        "CASE \n" +
                        "WHEN b.flag_checkpoint = 2 THEN 'CHECKER' \n" +
                        "WHEN b.flag_checkpoint IN (3,4) THEN 'SIGNER' \n" +
                        "ELSE 'MAKER' END AS statusMCS ,\n" +
                        "b.nomor_persetujuan,\n" +
                        "b.tgl_keputusan,\n" +
                        "b.nilai_persetujuan,\n" +
                        "CASE WHEN (c.flag_kirim = 1) THEN 'TERKIRIM' \n" +
                        "ELSE 'BELUM KIRIM' END as statusClaimFeedback,\n" +
                        "d.date_created as tanggalClaimFeedback,\n" +
                        "c.nominal_persetujuan,\n" +
                        "b.no_giro_klaim\n" +
                        "FROM opmt_pengajuan_tamp a\n" +
                        "INNER JOIN pengajuan_klaim b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "LEFT JOIN tbl_feedback_klaim_request c ON c.id_pengajuan_klaim = b.id_pengajuan_klaim\n" +
                        "LEFT JOIN tbl_feedback_klaim_response d ON d.id_tbl_feedback_klaim_request = c.id_tbl_feedback_klaim_request\n" +
                        "WHERE 1=1\n" +
                        "AND b.id_dd_bank = 15\n" +
                        "AND a.no_rekening = ?\n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC;";
                break;
            case "noPeserta" :
                sql = "SELECT\n" +
                        "a.nomor_peserta,\n" +
                        "b.date_created AS periodeKlaimDiterima,\n" +
                        "b.nilai_tuntutan_klaim,\n" +
                        "b.tunggakan_pokok,\n" +
                        "b.tunggakan_bunga,\n" +
                        "b.tunggakan_denda,\n" +
                        "b.nilai_payoff_total,\n" +
                        "CASE WHEN b.persetujuan_klaim = 2 THEN 'SETUJU' ELSE 'TOLAK' END AS keputusanKlaim,\n" +
                        "CASE \n" +
                        "WHEN b.flag_checkpoint = 2 THEN 'CHECKER' \n" +
                        "WHEN b.flag_checkpoint IN (3,4) THEN 'SIGNER' \n" +
                        "ELSE 'SIGNER' END AS statusMCS ,\n" +
                        "b.nomor_persetujuan,\n" +
                        "b.tgl_keputusan,\n" +
                        "b.nilai_persetujuan,\n" +
                        "CASE WHEN (c.flag_kirim = 1) THEN 'TERKIRIM' \n" +
                        "ELSE 'BELUM KIRIM' END as statusClaimFeedback,\n" +
                        "d.date_created as tanggalClaimFeedback,\n" +
                        "c.nominal_persetujuan,\n" +
                        "b.no_giro_klaim\n" +
                        "FROM opmt_pengajuan_tamp a\n" +
                        "INNER JOIN pengajuan_klaim b ON a.id_opmt_pengajuan_tamp = b.id_opmt_pengajuan_tamp\n" +
                        "LEFT JOIN tbl_feedback_klaim_request c ON c.id_pengajuan_klaim = b.id_pengajuan_klaim\n" +
                        "LEFT JOIN tbl_feedback_klaim_response d ON d.id_tbl_feedback_klaim_request = c.id_tbl_feedback_klaim_request\n" +
                        "WHERE 1=1\n" +
                        "AND b.id_dd_bank = 15\n" +
                        "AND a.nomor_peserta = ?\n" +
                        "ORDER BY a.id_opmt_pengajuan_tamp DESC;";
                break;
            default:
                sql = "Tidak ada data";
                break;
        }

        List<Map<String, Object>> result;

        if(!sql.equals("Tidak ada data")) {
            result = jdbcTemplate.queryForList(sql, noRekening);
        }else{
            result = new ArrayList<>();
        }

        if (!result.isEmpty()){
            return result.get(0);
        }else{
            return null;
        }
    }
}
